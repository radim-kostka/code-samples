const utils = require('util');
const cryptoJS = require('crypto-js');



const ConnectorConfigModel = require('../models/connectorconfig').model;
const  {BinanceConnector} = require('../connectors/binance');
const  {KuCoinConnector} = require('../connectors/kucoin');
const {BittrexConnector} = require('../connectors/bittrex');

const logger = require('../config/logger');


module.exports.saveConnectorConfigs = async (user, connectorConfigs) => {
    logger.debug('Save crypto holdings');
    connectorConfigs.forEach(element => {
        logger.debug(utils.inspect(element));
        element.userId = user._id;
    });
    logger.debug('Saving :'+utils.inspect(connectorConfigs));
    //await cryptoHoldingModel.create(cryptoHoldings);

    let toUpsert = [];
    connectorConfigs.forEach(e => {
        logger.debug(utils.inspect(e));
        logger.debug('1');
        let key = cryptoJS.AES.encrypt(e['key'], process.env.SERVER_SECRET).toString();
        let secret = cryptoJS.AES.encrypt(e['secret'], process.env.SERVER_SECRET).toString();
        let passphrase = cryptoJS.AES.encrypt(e['passphrase'], process.env.SERVER_SECRET).toString();

        e['key'] = key;
        e['secret'] = secret;
        e['passphrase'] = passphrase;


        logger.debug(utils.inspect(e));

        toUpsert.push(ConnectorConfigModel.findOneAndUpdate({systemId : e.systemId, 'userId' : user._id}, e, {upsert: true, new: true, runValidators: true}));
        logger.debug('4');
    });

    return await Promise.all(toUpsert);

};

const getConnectorConfigs = async (user) => {
    let connectors = await ConnectorConfigModel.find({'userId': user._id});
    return connectors.map(c => {
        c['key'] = cryptoJS.AES.decrypt(c['key'], process.env.SERVER_SECRET).toString(cryptoJS.enc.Utf8);
        c['secret'] = cryptoJS.AES.decrypt(c['secret'], process.env.SERVER_SECRET).toString(cryptoJS.enc.Utf8);
        c['passphrase'] = cryptoJS.AES.decrypt(c['passphrase'], process.env.SERVER_SECRET).toString(cryptoJS.enc.Utf8);
        return c;
    });
};

module.exports.getConnectorConfigs = getConnectorConfigs;

module.exports.deleteConnectorConfigs = async (user, connectorConfigs) => {
    return await ConnectorConfigModel.deleteMany({'userId' : user._id, 'systemId' : connectorConfigs.map(e => e.systemId)});
};




