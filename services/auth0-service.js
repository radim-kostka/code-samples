const jwt = require('express-jwt');
const jwtAuthz = require('express-jwt-authz');
const jwksRsa = require('jwks-rsa');
const util = require('util');
const User = require('../models/user').model;
const {UserService} = require('./user-service');


const logger = require('../config/logger');
const {AMError} = require("../config/am-error");

// Authentication middleware. When used, the
// Access Token must exist and be verified against
// the Auth0 JSON Web Key Set
const verifyJWTToken = jwt({
    // Dynamically provide a signing key
    // based on the kid in the header and
    // the signing keys provided by the JWKS endpoint.
    secret: jwksRsa.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: process.env.AUTH0_JWKS_URI
    }),

    // Validate the audience and the issuer.
    audience: process.env.AUTH0_AUDIENCE,
    issuer: process.env.AUTH0_ISSUER,
    algorithms: ['RS256']
});

const verifyUser = async (req, res, next) => {
    try {
        const user = await UserService.getOrCreateUser(req.user.sub);

        //allow /user endpoint even for expierd users, all other endpoints will reject expired users
        if (req.url !== '/user' && ( user.subscriptionEnd < new Date() || user.subscriptionEnd === undefined)) {
            let amError;
            if (user.subscriptionType === 'trial') {
                logger.debug('Trial period expired');
                amError = new AMError('Expired-001', 'Trial expired', 'Subscribe / renew subscription');
            } else {
                logger.debug('Subscription period expired');
                amError = new AMError('Expired-002', 'Subscription expired', 'Subscribe / renew subscription');
            }
            res.status(402).json(amError);
            return;
        }



        req.user = {...req.user, ...user};
        logger.debug(util.inspect(req.user));
        next();
    } catch (e) {
        logger.error(e);
        res.status(500).json(e);
    }
}

module.exports.verifyJWTToken = verifyJWTToken;
module.exports.verifyUser = verifyUser;
