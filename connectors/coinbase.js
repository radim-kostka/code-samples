const crypto = require('crypto');
const axios = require('axios');
const dotenv = require('dotenv').config();
const utils = require('util');
const cryptoJS = require('crypto-js');
const ConnectorConfigModel = require('../models/connectorconfig').model;
const UserModel = require('../models/user').model;

const logger = require('../config/logger');
const {AMError} = require("../config/am-error");

class CoinbaseConnector {

    constructor(accessToken, refreshToken, expiresAt, userId) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.tokenExpiration = expiresAt;
        this.userId = userId;
    }



    async exchangeRefreshToken() {
        const {COINBASE_GET_TOKEN_URL, COINBASE_CLIENT_SECRET, COINBASE_CLIENT_ID, SERVER_SECRET} = process.env;
        const url = `${COINBASE_GET_TOKEN_URL}`;
        const body = {
            grant_type : 'refresh_token',
            refresh_token : this.refreshToken,
            client_id : COINBASE_CLIENT_ID,
            client_secret: COINBASE_CLIENT_SECRET,
        };

        try {
            const response = await axios.post(url, body);
            const {access_token, token_type, expires_in, created_at, refresh_token, scope} = response.data;
            this.accessToken = access_token;
            this.refreshToken = refresh_token;
            //set expiration as 5 mins before actual expiration - to cater for last seconds multi-page requests
            //expiration retuirned from Coinbase is in seconds - need to multiply by 1000 to get valid date in millis
            this.tokenExpiration = (created_at + expires_in - 5 * 60) * 1000;

            let toUpsert = {
                userId: this.userId,
                systemId: 'coinbase',
                key: null,
                secret: null,
                passphrase : null,
                accessToken : cryptoJS.AES.encrypt(access_token, SERVER_SECRET).toString(),
                refreshToken : cryptoJS.AES.encrypt(refresh_token, SERVER_SECRET).toString(),
                tokenExpiration : new Date(this.tokenExpiration )
            };

            await ConnectorConfigModel.findOneAndUpdate({systemId : toUpsert.systemId, userId : this.userId},
                toUpsert,
                {upsert: true, new: true, runValidators: true});


        } catch (e) {
            logger.error(e);
            throw e;
        }

    }

    async getAccountsAndBalances() {
        const {COINBASE_API_BASE_URL, COINBASE_ACCOUNTS_URI, COINBASE_API_VERSION} = process.env;
        let accountsUrl = `${COINBASE_API_BASE_URL}/${COINBASE_ACCOUNTS_URI}?limit=100`;

        try {
            if (new Date().getTime() >= this.tokenExpiration) {
                logger.debug('refreshing token');
                await this.exchangeRefreshToken();
            }

            const balances = [];
            let nextPage = true;

            while (nextPage) {

                const response = await axios.get(accountsUrl, {
                    headers: {'Authorization': `Bearer ${this.accessToken}`, 'CB-VERSION': COINBASE_API_VERSION}
                });
                const {data} = response;
                balances.push(...data.data);
                nextPage = data['pagination']['next_uri'];
                accountsUrl = `${COINBASE_API_BASE_URL}${nextPage}`;
                logger.debug('Balances - next uri ' + accountsUrl);
            }
            return balances;
        } catch (e) {
            logger.error(e);
            throw e;
        }
    }

    async getTransactionsForAccount(accountId) {
        const {COINBASE_API_BASE_URL, COINBASE_ACCOUNTS_URI, COINBASE_API_VERSION} = process.env;
        let transUrl = `${COINBASE_API_BASE_URL}/${COINBASE_ACCOUNTS_URI}/${accountId}/transactions?limit=100`;

        try {
            if (new Date().getTime() >= this.tokenExpiration) {
                logger.debug('refreshing token');
                await this.exchangeRefreshToken();
            }

            const transactions = [];
            let nextPage = true;

            while (nextPage) {

                const response = await axios.get(transUrl, {
                    headers: {'Authorization': `Bearer ${this.accessToken}`, 'CB-VERSION': COINBASE_API_VERSION}
                });
                const {data} = response;
                transactions.push(...data.data);
                nextPage = data['pagination']['next_uri'];
                transUrl = `${COINBASE_API_BASE_URL}${nextPage}`;
                logger.debug('Transactions - next uri ' + transUrl);

            }
            return transactions;
        } catch (e) {
            logger.error(e);
            throw e;
        }
    }

    async getBalancesAndTransactions() {
        try {
            const accounts = await this.getAccountsAndBalances();
            for (let account of accounts) {
                try {
                    account.transactions = await this.getTransactionsForAccount(account.id);
                } catch (e) {
                    if (e.response.status == 404) {
                        //coin base sometimes returns token code instead of account id
                        //and then when requesting transactions for that account, it returns 404
                        //just ignore the error and return empty transactions list
                        account.transactions = [];
                    } else {
                        throw e;
                    }
                }
            }
            return accounts;
        } catch(e) {
            logger.debug(e);
            throw e;
        }
    }


}

const exchangeCodeForTokens = async (code) => {

    const {COINBASE_GET_TOKEN_URL, COINBASE_CLIENT_SECRET, COINBASE_CLIENT_ID, COINBASE_REGISTERED_CALLBACK_URI} = process.env;
    const url = `${COINBASE_GET_TOKEN_URL}`;
    const body = {
        grant_type : 'authorization_code',
        code : code,
        client_id : COINBASE_CLIENT_ID,
        client_secret: COINBASE_CLIENT_SECRET,
        redirect_uri : COINBASE_REGISTERED_CALLBACK_URI
    };

    try {
        const response = await axios.post(url, body);
        const {access_token, token_type, expires_in, created_at, refresh_token, scope} = response.data;
        console.log(response.data);
        return response.data;
    } catch (e) {
        logger.error(e);
        throw e;
    }
}

//returns encrypted userId
const generateRegisterRequestToken = (username) => {
    return Buffer.from(cryptoJS.AES.encrypt(username, process.env.SERVER_SECRET).toString()).toString('base64');
}

const registerConnector = async (registerRequestToken, regInfo) => {
    const requestUserId = cryptoJS.AES.decrypt(Buffer.from(registerRequestToken,'base64').toString(), process.env.SERVER_SECRET).toString(cryptoJS.enc.Utf8);
    const user = await UserModel.findOne({username : requestUserId});

    if (!user) {
        throw new AMError('coinbase-001', 'UserId does not match requestToken user id.', 'Possible attach attempt', 401);
    }

    const {access_token, expires_in, refresh_token, created_at} = regInfo;
    const {SERVER_SECRET} = process.env;

    let toUpsert = {
        userId: user._id,
        systemId: 'coinbase',
        key: null,
        secret: null,
        passphrase : null,
        accessToken : cryptoJS.AES.encrypt(access_token, SERVER_SECRET).toString(),
        refreshToken : cryptoJS.AES.encrypt(refresh_token, SERVER_SECRET).toString(),
        //created_at and expires_in are returned in seconds not in millis, need to convert to valid date
        tokenExpiration : new Date((created_at + expires_in) * 1000 )
    };

    return ConnectorConfigModel.findOneAndUpdate({systemId : toUpsert.systemId, userId : user._id},
        toUpsert,
        {upsert: true, new: true, runValidators: true});
}

CoinbaseConnector.generateRegisterRequestToken = generateRegisterRequestToken;
CoinbaseConnector.registerConnector = registerConnector;
CoinbaseConnector.exchangeCodeForTokens = exchangeCodeForTokens;

module.exports.CoinbaseConnector  = CoinbaseConnector;

