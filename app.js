const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');
const dotenv = require('dotenv').config();
const winston = require('winston');
const cors = require('cors');

const logger = require('./config/logger');
const marketDataCaches = require('./config/market-data-caches');
const {verifyJWTToken, verifyUser} = require('./services/auth0-service');

const indexRouter = require('./routes/index-router');
const usersRouter = require('./routes/user-router');
const cryptoRouter = require('./routes/crypto-router');
const connectorRouter = require('./routes/connector-router');
const holdingRouter = require('./routes/holdings-router');
const marketDataRouter = require('./routes/market-data-router');
const auth0Router = require('./routes/auth0-router');
const coinbaseOauthRouter = require('./routes/coinbase-oauth-router');



const app = express();

app.use(cors());

app.all('*', (req, res, next) => {
    if (req.secure) {
        return next();
    } else {
        console.log('redirecting');
        res.redirect(307, 'https://' + req.hostname + ':' + app.get('secPort') + req.url);
    }

});

app.use(morgan('combined'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/user', usersRouter);
app.use('/crypto', verifyJWTToken, verifyUser, cryptoRouter);
app.use('/connector', verifyJWTToken, verifyUser,connectorRouter);
app.use('/holding', verifyJWTToken, verifyUser, holdingRouter);
app.use('/market-data', verifyJWTToken, verifyUser, marketDataRouter);
app.use('/auth0', verifyJWTToken, verifyUser, auth0Router);
app.use('/coinbase-oauth', coinbaseOauthRouter);


mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false });
mongoose.connection.on('error', (err) => {
    console.error(err);
});



module.exports = app;
